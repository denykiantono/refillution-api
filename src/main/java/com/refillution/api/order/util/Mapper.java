package com.refillution.api.order.util;

import com.refillution.api.order.entity.Order;
import com.refillution.api.order.enums.OrderStatus;
import com.refillution.api.order.request.CreateOrderRequest;
import com.refillution.api.order.request.UpdateOrderRequest;
import com.refillution.api.order.response.CreateOrderResponse;
import com.refillution.api.order.response.FindOrderByIdResponse;
import com.refillution.api.order.response.FindOrderResponse;
import com.refillution.api.order.response.UpdateOrderResponse;
import org.springframework.beans.BeanUtils;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

public class Mapper {

  public static Order mapToEntity(CreateOrderRequest request) {
    Order order = new Order();
    BeanUtils.copyProperties(request, order);
    order.setStatus(OrderStatus.ON_PROGRESS);
    order.setPrice(order.getProductPrice().multiply(BigDecimal.valueOf(order.getQuantity())));
    return order;
  }

  public static Order mapToEntity(UpdateOrderRequest request, Order order) {
    Order updatedOrder = new Order();
    BeanUtils.copyProperties(order, updatedOrder);

    if (Objects.nonNull(request.getStatus())) {
      updatedOrder.setStatus(request.getStatus());
    }

    if (Objects.nonNull(request.getDeliveryDate())) {
      updatedOrder.setDeliveryDate(request.getDeliveryDate());
    }

    if (Objects.nonNull(request.getDeliveryTime())) {
      updatedOrder.setDeliveryTime(request.getDeliveryTime());
    }

    return updatedOrder;
  }

  public static CreateOrderResponse mapToCreateOrderResponse(Order order) {
    CreateOrderResponse response = new CreateOrderResponse();
    BeanUtils.copyProperties(order, response);
    return response;
  }

  public static FindOrderByIdResponse mapToFindOrderByIdResponse(Order order) {
    FindOrderByIdResponse response = new FindOrderByIdResponse();
    BeanUtils.copyProperties(order, response);
    return response;
  }

  public static FindOrderResponse mapToFindOrderResponse(List<Order> orders) {
    FindOrderResponse response = new FindOrderResponse();
    orders.forEach(order -> response.addOrder(mapToFindOrderByIdResponse(order)));
    return response;
  }

  public static UpdateOrderResponse mapToUpdateOrderResponse(Order order) {
    UpdateOrderResponse response = new UpdateOrderResponse();
    BeanUtils.copyProperties(order, response);
    return response;
  }
}
