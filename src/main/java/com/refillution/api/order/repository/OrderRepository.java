package com.refillution.api.order.repository;

import com.refillution.api.order.entity.Order;
import com.refillution.api.order.enums.OrderStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderRepository extends JpaRepository<Order, Integer> {
  List<Order> findByStatus(OrderStatus status);
}
