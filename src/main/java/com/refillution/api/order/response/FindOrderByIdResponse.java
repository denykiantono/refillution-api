package com.refillution.api.order.response;

import com.refillution.api.order.enums.OrderStatus;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
public class FindOrderByIdResponse {
  private int id;
  private String productId;
  private BigDecimal productPrice;
  private int amount;
  private int quantity;
  private BigDecimal price;
  private OrderStatus status;
  private LocalDate deliveryDate;
  private String deliveryTime;
}
