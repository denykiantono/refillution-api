package com.refillution.api.order.response;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class FindOrderResponse {
  private List<FindOrderByIdResponse> orders;

  public FindOrderResponse() {
    orders = new ArrayList<>();
  }

  public void addOrder(FindOrderByIdResponse order) {
    this.orders.add(order);
  }
}
