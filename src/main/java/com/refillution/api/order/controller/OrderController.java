package com.refillution.api.order.controller;

import com.refillution.api.order.enums.OrderStatus;
import com.refillution.api.order.request.CreateOrderRequest;
import com.refillution.api.order.request.UpdateOrderRequest;
import com.refillution.api.order.response.CreateOrderResponse;
import com.refillution.api.order.response.FindOrderByIdResponse;
import com.refillution.api.order.response.FindOrderResponse;
import com.refillution.api.order.response.UpdateOrderResponse;
import com.refillution.api.order.service.OrderService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/order")
public class OrderController {

  private OrderService orderService;

  public OrderController(OrderService orderService) {
    this.orderService = orderService;
  }

  @PostMapping
  Mono<CreateOrderResponse> save(@RequestBody Mono<CreateOrderRequest> request) {
    return orderService.save(request);
  }

  @GetMapping("/{id}")
  Mono<FindOrderByIdResponse> findById(@PathVariable int id) {
    return this.orderService.findById(id);
  }

  @GetMapping
  Mono<FindOrderResponse> find(@RequestParam(required = false) OrderStatus status) {
    return this.orderService.find(status);
  }

  @PatchMapping("/{id}")
  Mono<UpdateOrderResponse> update(@PathVariable int id, @RequestBody UpdateOrderRequest request) {
    return this.orderService.update(id, request);
  }
}
