package com.refillution.api.order.entity;

import com.refillution.api.order.enums.OrderStatus;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@Entity
@Table(name = "orders")
public class Order {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;
  private String productId;
  private BigDecimal productPrice;
  private int amount;
  private int quantity;
  private BigDecimal price;
  private OrderStatus status;
  private LocalDate deliveryDate;
  private String deliveryTime;
}
