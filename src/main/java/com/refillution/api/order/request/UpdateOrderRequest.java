package com.refillution.api.order.request;

import com.refillution.api.order.enums.OrderStatus;
import lombok.Data;

import java.time.LocalDate;

@Data
public class UpdateOrderRequest {
  private OrderStatus status;
  private LocalDate deliveryDate;
  private String deliveryTime;
}
