package com.refillution.api.order.request;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
public class CreateOrderRequest {
  private String productId;
  private BigDecimal productPrice;
  private int amount;
  private int quantity;
  private LocalDate deliveryDate;
  private String deliveryTime;
}
