package com.refillution.api.order.enums;

public enum OrderStatus {
  ON_PROGRESS,
  CONFIRMED,
  PAID,
  DONE
}
