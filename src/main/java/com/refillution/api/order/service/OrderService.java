package com.refillution.api.order.service;

import com.refillution.api.order.enums.OrderStatus;
import com.refillution.api.order.request.CreateOrderRequest;
import com.refillution.api.order.request.UpdateOrderRequest;
import com.refillution.api.order.response.CreateOrderResponse;
import com.refillution.api.order.response.FindOrderByIdResponse;
import com.refillution.api.order.response.FindOrderResponse;
import com.refillution.api.order.response.UpdateOrderResponse;
import reactor.core.publisher.Mono;

public interface OrderService {
  Mono<CreateOrderResponse> save(Mono<CreateOrderRequest> request);
  Mono<FindOrderByIdResponse> findById(int id);
  Mono<FindOrderResponse> find(OrderStatus status);
  Mono<UpdateOrderResponse> update(int id, UpdateOrderRequest request);
}
