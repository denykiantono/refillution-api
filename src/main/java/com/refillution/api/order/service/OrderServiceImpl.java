package com.refillution.api.order.service;

import com.refillution.api.order.enums.OrderStatus;
import com.refillution.api.order.repository.OrderRepository;
import com.refillution.api.order.request.CreateOrderRequest;
import com.refillution.api.order.request.UpdateOrderRequest;
import com.refillution.api.order.response.CreateOrderResponse;
import com.refillution.api.order.response.FindOrderByIdResponse;
import com.refillution.api.order.response.FindOrderResponse;
import com.refillution.api.order.response.UpdateOrderResponse;
import com.refillution.api.order.util.Mapper;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.util.Objects;
import java.util.Optional;

@Service
public class OrderServiceImpl implements OrderService {

  private OrderRepository orderRepository;

  public OrderServiceImpl(OrderRepository orderRepository) {
    this.orderRepository = orderRepository;
  }

  @Override
  public Mono<CreateOrderResponse> save(Mono<CreateOrderRequest> request) {
    return request.map(Mapper::mapToEntity)
        .map(this.orderRepository::save)
        .map(Mapper::mapToCreateOrderResponse)
        .subscribeOn(Schedulers.boundedElastic());
  }

  @Override
  public Mono<FindOrderByIdResponse> findById(int id) {
    return Mono.fromCallable(() -> this.orderRepository.findById(id))
        .filter(Optional::isPresent)
        .map(Optional::get)
        .map(Mapper::mapToFindOrderByIdResponse)
        .subscribeOn(Schedulers.boundedElastic());
  }

  @Override
  public Mono<FindOrderResponse> find(OrderStatus status) {
    return Mono.fromCallable(() -> Objects.isNull(status) ?
            this.orderRepository.findAll() :
            this.orderRepository.findByStatus(status))
        .map(Mapper::mapToFindOrderResponse)
        .subscribeOn(Schedulers.boundedElastic());
  }

  @Override
  public Mono<UpdateOrderResponse> update(int id, UpdateOrderRequest request) {
    return Mono.fromCallable(() -> this.orderRepository.findById(id))
        .filter(Optional::isPresent)
        .map(Optional::get)
        .map(order -> Mapper.mapToEntity(request, order))
        .map(this.orderRepository::save)
        .map(Mapper::mapToUpdateOrderResponse)
        .subscribeOn(Schedulers.boundedElastic());
  }
}
