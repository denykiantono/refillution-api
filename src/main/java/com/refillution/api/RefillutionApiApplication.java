package com.refillution.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RefillutionApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(RefillutionApiApplication.class, args);
	}

}
